﻿using LivrariaLinq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqVideo
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Aluno> Alunos = ListaDeAlunos.LoadAlunos(); //lista de alunos é preenchida com a lista de alunos da classe intermedia "LIstaDeAlunos"

            //ordenar por apelido

            //Alunos = Alunos.OrderBy(x => x.Apelido).ToList(); //nos temos uma lista generica e o linq cria uma lista enumerica, por isso temos de conver para uma lista das nossas ".ToList()"

            //Alunos = Alunos.OrderByDescending(x => x.Apelido).ToList(); //ordem descendente

            Alunos = Alunos.OrderByDescending(x => x.Apelido).ThenByDescending(x => x.DisciplinasFeitas).ToList();

            //Alunos = Alunos.Where(x => x.DisciplinasFeitas > 10 && x.DataNascimento.Month == 3).ToList(); //so vai mostrar que tiver nascide em marco e mais de 10 disc feitas

            foreach (var aluno in Alunos)
            {
                Console.WriteLine($"{aluno.PrimeiroNome} {aluno.Apelido} ({aluno.DataNascimento.ToShortDateString()}) Disciplinas Feitas :{ aluno.DisciplinasFeitas}");
            }

            int TotalDeDisciplinasFeitas = Alunos.Sum(x => x.DisciplinasFeitas);
            double MediaDeDisciplinasFeitas = Alunos.Average(x => x.DisciplinasFeitas);

            Console.WriteLine($"Total de disciplinas feitas: {TotalDeDisciplinasFeitas}");
            Console.WriteLine($"Media de disciplinas feitas: {MediaDeDisciplinasFeitas:N2}");//:N2 pegado ao variavel da o numro de casas decimais

            TotalDeDisciplinasFeitas = Alunos.Where(x => x.DataNascimento.Month == 2).Sum(x => x.DisciplinasFeitas);
            MediaDeDisciplinasFeitas = Alunos.Where(x => x.DataNascimento.Month == 2).Average(x => x.DisciplinasFeitas);

            Console.WriteLine($"Total de disciplinas feitas: {TotalDeDisciplinasFeitas}");
            Console.WriteLine($"Media de disciplinas feitas: {MediaDeDisciplinasFeitas:N2}");

            Console.ReadKey();
        }
    }
}
